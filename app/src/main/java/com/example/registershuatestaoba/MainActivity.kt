package com.example.registershuatestaoba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var saxeli: EditText
    lateinit var gvari: EditText
    lateinit var email: EditText
    lateinit var paroli: EditText
    lateinit var registracia: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        saxeli = findViewById(R.id.saxeli)
        gvari = findViewById(R.id.gvari)
        email = findViewById(R.id.email)
        paroli = findViewById(R.id.paroli)
        registracia = findViewById(R.id.registracia)

        registracia.setOnClickListener {

            val pas = paroli.text.toString()
            val mail = email.text.toString()

            if(pas.length < 8 && pas.length != 0){

                Toast.makeText(this,"პაროლი უდნა შეიცავდეს მინიმუმ 8 სიმბოლოს!", Toast.LENGTH_SHORT).show()
            }

            if(!pas.matches(".*[A-Z].*".toRegex())){
                Toast.makeText(this,"პაროლი უნდა შეიცავდეს მინიმუმ 1 დიდ ასოს!", Toast.LENGTH_SHORT).show()
            }

            if(!Patterns.EMAIL_ADDRESS.matcher(mail).matches()){
                Toast.makeText(this,"შეიყვანე სწორი მეილი!", Toast.LENGTH_SHORT).show()
            }
        }

    }
}